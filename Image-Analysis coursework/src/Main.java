import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * This project Will demonstrate different image compression and decompression methods
 * And we aim to reduce storage space whilst preserving image quality
 * @author Nahid
 *
 */
public class Main {

	Color background= new Color(204, 0, 0);// Background colour for JFrame

	//We will make the image label global for now(For easier access to clear and add Image
	JLabel lblImage;
	JButton btnCompression; //We are making it global for easier access
	JButton btnDecompression; //We are making it global for easier access
	JLabel lblFileSize; //Will be used to determine FileSize
	JLabel lblFileSizeAfter;//Will be used to display FileSize after changes


	public Main() {

		JFrame frame = new JFrame("Image Analysis");

		frame.setSize(1200, 1000);
		frame.setVisible(true);
		//Will be windows 10 specific
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null); // We do not want to set layout

		lblImage= new JLabel();//Initilaise our image Label

		//Initilaise our label File size
		lblFileSize = new JLabel("Currently no Image has been loaded in:"); //Initilising JLabel which will inform users about size of File
		lblFileSize.setBounds(30, 500, 200, 50);
		frame.add(lblFileSize); //Adds Label to Frame
		
		lblFileSizeAfter = new JLabel("Currently Nothing to display");
		lblFileSizeAfter.setBounds(30, 600, 200, 50);
		frame.add(lblFileSizeAfter);



		//We will add a Open Image File button
		JButton btnFileOpener = new JButton("Choose an Image");
		btnFileOpener.setBounds(500, 50, 170, 50);//Sets Bounds of Image
		frame.add(btnFileOpener); //Adds button to our JFrame

		//Need to add action listener for the btnFile Opener
		btnFileOpener.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String imagePath= openFileExplorer();
				System.out.println("The Image pathway is :"+ imagePath);
				if(imagePath!= null) {//If we have an image path(TODO Need better checks to ensure it is an image)
					addImage(frame,imagePath);
				}
			}
		});
		// Compression and Decompression Button
		btnCompression = new JButton("Compression");
		btnCompression.setBounds(250, 50, 170, 50);//Sets Bounds of Image
		btnCompression.setEnabled(false);//We want it disabled if there is no loaded image

		frame.add(btnCompression); //Adds button to our JFrame

		btnDecompression = new JButton("Decompression");
		btnDecompression.setBounds(250, 100, 170, 50);	//Sets Bounds of Image
		btnDecompression.setEnabled(false);//We want it disabled if there is no loaded image

		frame.add(btnDecompression); //Adding decompression button to Frame


		//TODO Each button will display a menu and allow us to choose which Compression and decompression method to use
		btnCompression.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				// We will create a New Frame with certain features and may need to pass relevant data
				createCompressionMenu();//FIXME We may want to create this Jframe Menu before hand and just make it global and set Visible when need Just to be aware

			}
		});
		
		//Action listener to display Decompression Method
		btnDecompression.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//We will create a new Frame displaying decompression methods
				createDecompressionMenu();
				
			}
		});

		//TODO Once we display an Image we want to show users How much Bytes it currently is and How much bytes after compression/Decompression
		//TODO Option to save new images(Not hard with FileChooser

	}	

	public static void main(String[]args) {
		System.out.println("Testing GUYS+ anyrhing");
		System.out.println("Testing For Git");
		//Test
		new Main();
	}
	/**
	 * Saves the loaded Image as PNG
	 * FIXME however when we open dialog to save we have to save as filename.png ourseleves which we need to modify ourselves
	 */
	 private void saveAsPng() {
		 
		//Becuase our imageLabel is global we can easily access the Image
			Icon img =  lblImage.getIcon();//FIXME May have to check if there is a potential problem with casting(May have to pass imagePath and retrieve Image again)
			
			BufferedImage buffer =  new BufferedImage(img.getIconWidth(), img.getIconHeight(), BufferedImage.TYPE_3BYTE_BGR); //FIXME we may need to consider how we save BufferedImage(Third Parameter)
			
			Graphics g = buffer.createGraphics();
			img.paintIcon(null, g, 0, 0);//Saves Icon to BufferedImage ready to be saved in File
			g.dispose();//Saves Resource
			System.out.println("Checking if we did thios correctly : "+ buffer.getHeight());
		 
		 JFileChooser jfc = new JFileChooser();
		 int result= jfc.showSaveDialog(null);//This Shows Save Dialog
		 if(result==JFileChooser.APPROVE_OPTION) {
			 File file = jfc.getSelectedFile();
			 try {
				ImageIO.write(buffer, "png", new File(file.getAbsolutePath()));
				//Once we have saved the file we can pass the file path of the new file and get its size
				int fileNewSize = retrieveImageSizeInBytes(file.getAbsolutePath());
				System.out.println(" The new File Size is : "+ fileNewSize+" bytes");
				lblFileSizeAfter.setText("The size of the new image is : "+ fileNewSize+" bytes");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
	 }
	/**
	 * Creates a menu for Decompression method
	 */
	private void createDecompressionMenu() {
		JFrame compressionFrame = new JFrame("Decompression Choice");

		compressionFrame.setSize(500, 500);
		compressionFrame.setVisible(true);
		//Will be windows 10 specific
		compressionFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		compressionFrame.setLayout(null); // We do not want to set layout
	}
	
	/**
	 * Creates a menu to let us choose which compression method to use
	 */
	private void createCompressionMenu() {
		JFrame compressionFrame = new JFrame("Compression Choice");

		compressionFrame.setSize(500, 500);
		compressionFrame.setVisible(true);
		//Will be windows 10 specific
		compressionFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		compressionFrame.setLayout(null); // We do not want to set layout

		//Add the possible compression choices button
		
		 JButton btnSaveAsPng= new JButton("Save as Png"); //Saving as png is a form of loseless copression
		 btnSaveAsPng.setBounds(30, 30, 150, 50);//Setting bounds of button
		 compressionFrame.add(btnSaveAsPng);//Adding button to our compresssion menu
		 
		 //Action Listener for btnSavePng
		 btnSaveAsPng.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//Lets see if we can ImAGEiCON AS bufferedImage
				System.out.println("Testing BUTTON");
				
				saveAsPng();
			}
		});
		
	}


	/**
	 * THIS WORKS
	 * This method returns the length in bytes the size of the ImageFile(Can be used on any file)
	 * @param imagePath absolute path to Image
	 * @return int that is the length of bytes of the image path passed
	 */
	private int retrieveImageSizeInBytes( String imagePath) {
		File file = new File(imagePath);//Creates File with the passed imagePath
		int sizeOfFile = (int) file.length();//Returns size of file in bytes
		return sizeOfFile;
	}

	/**
	 * This method will display image in our JFrame 
	 * @param frame Our Main JFrame which the image will be displayed on
	 * @param imagePath The image path for the image we will display
	 */
	private void addImage(JFrame frame, String imagePath) {
		ImageIcon image = new ImageIcon(imagePath);//Creates Image icon based with the path we pass
		//lblImage= new JLabel(image);
		lblImage.setIcon(image);
		lblImage.setBounds(400, 400, 400, 400); //TODO We should get Height and width of Image so we can display it Properly
		lblImage.setVisible(true);//We want to display the image
		frame.add(lblImage);//We add Image Icon to our Frame
		//If there is an Image we can now enable our compression and decompression methods
		btnCompression.setEnabled(true);//Making them usable
		btnDecompression.setEnabled(true);//Making them usable

		// We can also get File size(As we already passed image Path we can reconstruct File and Get Length
		int size=retrieveImageSizeInBytes(imagePath);
		// Once we get Image Size it is best to Display this to Users
		lblFileSize.setText("The size of this file is :"+ size+" bytes");




	}

	/**
	 * This method opens a file chooser for the user to choose a file
	 * @return path of file(Does not check if the file is an image
	 */
	private String openFileExplorer() {
		JFileChooser fileChooser = new JFileChooser();//Creates a FileChooser
		int result= fileChooser.showOpenDialog(null); //Result if we selected or not(May need to pass in JFrame)
		if(result == fileChooser.APPROVE_OPTION) {
			String imagePath = fileChooser.getSelectedFile().getPath(); //Gets Image Pathway
			return imagePath;
		}
		//If you do not choose a file then we return null
		return null;
	}




}
